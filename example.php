<?php
// Se obtiene datos de ingreso del navegador
$user_agent = $_SERVER['HTTP_USER_AGENT'];

// Se pone como estandar zona horaria en Lima
date_default_timezone_set("America/Lima");

// Esta función recupera la ip de ingreso del usuario
function getRealIP() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
       
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
   
    return $_SERVER['REMOTE_ADDR'];
}

//valida desde que navegador ingresa el usuario
function getBrowser($user_agent){
 if(strpos($user_agent, 'MSIE') !== FALSE)
   return 'Internet explorer';
 elseif(strpos($user_agent, 'Edg') !== FALSE )
   return 'Microsoft Edge';
 elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
    return 'Internet explorer';
 elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
   return "Opera Mini";
 elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
   return "Opera";
 elseif(strpos($user_agent, 'Firefox') !== FALSE)
   return 'Mozilla Firefox';
 elseif(strpos($user_agent, 'Chrome') !== FALSE)
   return 'Google Chrome';
 elseif(strpos($user_agent, 'Safari') !== FALSE)
   return "Safari";
 else
   return 'No hemos podido detectar su navegador';
}
//se recupera el nombre del navegador
$navegador = getBrowser($user_agent);
// se recupera la ip del ususario
$ipusario = getRealIP();
// se obtiene la fecha de ingreso del usuario
$fechaActual = date('Y-m-d');

// se realiza la conexion al base de datos creada en mysql
$mysqli = new mysqli('127.0.0.1', 'daniel', '12345678', 'bdred');
$mysqli->set_charset("utf8");

// se hace la consulta sí es que el usuario ingreso el mismo día, la misma ip, y el mismo navegador
$res = $mysqli->query("SELECT * FROM usuario where ip_usuario='$ipusario' and fecha_usuario='$fechaActual' and navegador='$navegador'");

//se verifica la consulta anterio que sea inexistente para ingresar los datos de los usuarios
if(mysqli_num_rows($res)>0){
  echo "----".' <br/>';
}
else{
  // sí el usuario va entra por primera vez se ingresan los datos
  $query = "INSERT INTO usuario VALUES ('$ipusario','$fechaActual','$navegador')";
  $mysqli->query($query);
}

// se hace la consulta para ver cuantas veces se ingreso por cada navegador
$res2 = $mysqli->query("SELECT navegador from usuario");

$cadena = array();

while($f = $res2->fetch_object()){
    array_push($cadena, $f->navegador);
}
foreach (array_count_values($cadena) as $key=>$item){
    echo "$key : $item <br>";
}
echo "Total: ".count($cadena).'<br>';

echo "------------- ip de usuarios que ingresaron -------------------".'<br/>';

$res3 = $mysqli->query("SELECT * from usuario");
while( $f = $res3->fetch_object() ){
    echo $f->ip_usuario.'<br>';
    echo $f->fecha_usuario.'<br>';
    echo $f->navegador.'<br>'.'<br>';

}
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title></title>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['navegador', 'usuarios'],
          <?php  
          // se pasan los datos de los navegadores y cantidades de ingreso
          //para que google chart haga los graficos de estadisticas.
          $mysqli = new mysqli('127.0.0.1', 'daniel', '12345678', 'bdred');
          $mysqli->set_charset("utf8");

          $res2 = $mysqli->query("SELECT navegador from usuario");

          $cadena = array();

          while($f = $res2->fetch_object()){
              array_push($cadena, $f->navegador);
          }

          foreach (array_count_values($cadena) as $key=>$item){

              echo "['".$key."',".$item."],";
              
          } ?>
          

      
        ]);

        var options = {
          title: 'Ingresos de usarios',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
</head>
<body>
<div id="piechart_3d" style="width: 900px; height: 500px;"></div>
</body>
</html>