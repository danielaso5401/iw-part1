from flask import Flask, render_template, request, redirect, url_for, flash
from flask import request
from datetime import datetime
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:12345678@127.0.0.1/vacuna'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
ma = Marshmallow(app)

class Paciente(db.Model):
    idpaciente = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    fecha = db.Column(db.DateTime)
    dni = db.Column(db.String(8))
    email = db.Column(db.String(100))

    def __init__(self, name, fecha, dni, email):
        self.name = name
        self.fecha = fecha
        self.dni = dni
        self.email = email

db.create_all()


class PacienteSchema(ma.Schema):
    class Meta:
        fields = ("idpaciente","name", "fecha","dni","email")

paciente_schema = PacienteSchema()
paciente_schemas = PacienteSchema(many=True)


@app.route('/create_paciente', methods=['POST'])
def create_cliente():
    print(request.json)
    name=request.json["name"]
    fecha=request.json["fecha"]
    dni=request.json["dni"]
    email=request.json["email"]
    fecha_dt = datetime.strptime(fecha, '%d/%m/%Y')
    new_paciente = Paciente(name,fecha_dt,dni,email)

    db.session.add(new_paciente)
    db.session.commit()

    return paciente_schema.jsonify(new_paciente)

@app.route('/get_paciente', methods=['GET'])
def get_paciente():
    all_pacientes = Paciente.query.all()
    result = paciente_schemas.dump(all_pacientes)
    return jsonify(result)

@app.route('/update_paciente/<ide>', methods=["PUT"])
def update_paciente(ide):

    name=request.json["name"]
    fecha=request.json["fecha"]
    dni=request.json["dni"]
    email=request.json["email"]
    paciente_id=Paciente.query.filter_by(idpaciente=ide).one()
    paciente_id.name=name
    paciente_id.fecha=datetime.strptime(fecha, '%d/%m/%Y')
    paciente_id.dni=dni
    paciente_id.email=email
    db.session.commit()
    return "acutalizacion correcta"

@app.route('/delete_paciente/<ide>', methods=['DELETE'])
def delete_paciente(ide):
    deletepaciente=Paciente.query.filter_by(idpaciente=ide).one()
    db.session.delete(deletepaciente)
    db.session.commit()
    return "eliminado correctamente"

@app.route("/")
def hello_world():
    return "<p>Hola</p>"

if __name__=="__main__":   
    app.run(port=3000, debug=True)